[![official](https://img.shields.io/badge/official-URL-orange.svg)](https://bitbucket.org/minotsu/android-library/src/master/databinding/)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://spdx.org/licenses/MIT.html)
![Bintray](https://img.shields.io/bintray/v/minotsu/maven/net.minotsu.android.databinding)
# DataBindingライブラリ

## 利用方法
implementation("net.minotsu.android:databinding:0.1.0")
