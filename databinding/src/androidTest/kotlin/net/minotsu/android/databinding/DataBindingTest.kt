/*******************************************************************************
 * © 2020 美濃通
 *
 * このソフトウェアはMITライセンスの下でリリースされています。
 * http://opensource.org/licenses/mit-license.php
 ******************************************************************************/

package net.minotsu.android.databinding

import androidx.fragment.app.FragmentFactory
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import org.assertj.core.api.Assertions
import org.junit.Test
import org.junit.runner.RunWith

@SmallTest
@RunWith(AndroidJUnit4::class)
class DataBindingTest {

    @Test
    fun dataBindingIsNullAfterDetachFragment() {
        val scenario =
            launchFragmentInContainer<DataBindingTestFragment>(factory = FragmentFactory())

        scenario.onFragment {
            it.parentFragmentManager.beginTransaction().detach(it).commitNow()
            Assertions.assertThat(it.view?.getTag("binding".hashCode())).isNull()
        }
    }
}
