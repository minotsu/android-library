/*******************************************************************************
 * © 2020 美濃通
 *
 * このソフトウェアはMITライセンスの下でリリースされています。
 * http://opensource.org/licenses/mit-license.php
 ******************************************************************************/

package net.minotsu.android.databinding

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import net.minotsu.android.databinding.databinding.DataBindingTestFragmentBinding

internal class DataBindingTestFragment : Fragment(R.layout.data_binding_test_fragment) {

    private val binding: DataBindingTestFragmentBinding by dataBinding()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.textView.isVisible = true
    }
}
