/*******************************************************************************
 * © 2020 美濃通
 *
 * このソフトウェアはMITライセンスの下でリリースされています。
 * http://opensource.org/licenses/mit-license.php
 ******************************************************************************/

@file:Suppress("RedundantVisibilityModifier")

package net.minotsu.android.databinding

import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

private class DataBindingPropertyInternal<T : ViewDataBinding> : ReadOnlyProperty<Fragment, T> {

    override fun getValue(thisRef: Fragment, property: KProperty<*>): T {
        val key = property.name.hashCode()

        @Suppress("UNCHECKED_CAST")
        return thisRef.requireView().getTag(key) as? T
            ?: checkNotNull(DataBindingUtil.bind<T>(thisRef.requireView())).also {
                it.lifecycleOwner = thisRef.viewLifecycleOwner
                thisRef.requireView().setTag(key, it)
            }
    }
}

/**
 * DataBindingの委譲プロパティを返す.
 *
 * FragmentでDataBindingの参照をプロパティとして持つとき、onDestroyViewでその参照を外さないとリークする。
 * それを意識する必要がないDataBindingの委譲プロパティを返す。
 *
 * @sample net.minotsu.android.databinding.DataBindingTestFragment
 */
public fun <T : ViewDataBinding> dataBinding(): ReadOnlyProperty<Fragment, T> =
    DataBindingPropertyInternal()
