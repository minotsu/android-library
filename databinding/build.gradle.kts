/*******************************************************************************
 * © 2020 美濃通
 *
 * このソフトウェアはMITライセンスの下でリリースされています。
 * http://opensource.org/licenses/mit-license.php
 ******************************************************************************/

plugins {
    id("plugin.android.library")
}

android {
    buildFeatures {
        dataBinding = true
    }
}

dependencies {
    implementation(Pkg.Androidx.Fragment.fragmentKtx)
    espressoImplementation(Pkg.Androidx.Fragment.fragmentTesting)
}
