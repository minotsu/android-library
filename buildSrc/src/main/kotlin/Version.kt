/*******************************************************************************
 * © 2020 美濃通
 *
 * このソフトウェアはMITライセンスの下でリリースされています。
 * http://opensource.org/licenses/mit-license.php
 ******************************************************************************/

class Version(
    private val major: Int,
    private val minor: Int,
    private val patch: Int
) {

    init {
        require(major >= 0) { "メジャーバージョンは0以上でなくてはなりません。" }
        require(minor in (0..99)) { "マイナーバージョンは0以上99以下でなくてはなりません。" }
        require(patch in (0..999)) { "パッチバージョンは0以上999以下でなくてはなりません。" }
    }

    /** バージョンコード. */
    val code: Int
        get() = major * 100000 + minor * 1000 + patch

    override fun toString(): String = "$major.$minor.$patch"
}
