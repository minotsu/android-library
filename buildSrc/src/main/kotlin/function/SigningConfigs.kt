/*******************************************************************************
 * © 2020 美濃通
 *
 * このソフトウェアはMITライセンスの下でリリースされています。
 * http://opensource.org/licenses/mit-license.php
 ******************************************************************************/

package function

import com.android.build.gradle.LibraryExtension
import java.io.File

internal fun LibraryExtension.signingConfigs(storeFile: File) {
    signingConfigs {
        defaultConfig.signingConfig?.apply {
            keyAlias = "DevelopmentAndroidSignKey"
            keyPassword = "minotsu"
            this.storeFile = storeFile
            storePassword = "minotsu"
        }
    }
}
