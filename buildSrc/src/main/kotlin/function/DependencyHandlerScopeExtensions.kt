/*******************************************************************************
 * © 2020 美濃通
 *
 * このソフトウェアはMITライセンスの下でリリースされています。
 * http://opensource.org/licenses/mit-license.php
 ******************************************************************************/

package function

import Pkg
import org.gradle.api.artifacts.ExternalModuleDependency
import org.gradle.kotlin.dsl.DependencyHandlerScope
import org.gradle.kotlin.dsl.accessors.runtime.addDependencyTo

fun DependencyHandlerScope.robolectric() {
    addDependencyTo<ExternalModuleDependency>(
        this,
        "testImplementation",
        Pkg.Org.Robolectric.robolectric
    ) {
        exclude(mapOf("group" to "com.google.auto.service", "module" to "auto-service"))
    }
}

fun DependencyHandlerScope.test() {
    for (pkg in listOf(
        Pkg.Androidx.Test.coreKtx,
        Pkg.Androidx.Test.rules,
        Pkg.Androidx.Test.Ext.junitKtx,
        Pkg.Org.Assertj.assertjCore,
        Pkg.Androidx.Test.Espresso.espressoCore
    )) {
        add("testImplementation", pkg)
        robolectric()
        add("androidTestImplementation", pkg)
        add("androidTestImplementation", Pkg.Androidx.Test.Uiautomator.uiautomator)
        add("androidTestUtil", Pkg.Androidx.Test.orchestrator)
    }
}
