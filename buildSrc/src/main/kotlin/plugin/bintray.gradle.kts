/*******************************************************************************
 * © 2020 美濃通
 *
 * このソフトウェアはMITライセンスの下でリリースされています。
 * http://opensource.org/licenses/mit-license.php
 ******************************************************************************/

package plugin

import Version

internal val publicationName = "maven"
internal val moduleArtifactId = property("artifactId") as String
internal val majorVersion = property("majorVersion").toString().toInt()
internal val minorVersion = property("minorVersion").toString().toInt()
internal val patchVersion = property("patchVersion").toString().toInt()
internal val moduleVersion = Version(majorVersion, minorVersion, patchVersion)
internal val moduleVcsUrl = property("vcsUrl") as String
internal val moduleWebsiteUrl = property("websiteUrl") as String
internal val bintrayUser = property("bintray.user") as String
internal val bintrayKey = property("bintray.key") as String

plugins {
    id("com.android.library")
    id("plugin.dokka")
    `maven-publish`
    signing
    id("com.jfrog.bintray")
}

tasks {
    register<Jar>("androidSourcesJar") {
        archiveClassifier.set("sources")
        archiveVersion.set("$moduleVersion")

        android.sourceSets
            .filter { listOf("main", "release").contains(it.name) }
            .forEach { from(it.java.srcDirs) }
    }

    register<Jar>("androidKdocsJar") {
        dependsOn("dokka")
        archiveClassifier.set("kdoc")
        archiveVersion.set("$moduleVersion")
        from(dokka.get().outputDirectory)
    }
}

afterEvaluate {
    publishing {
        publications {
            create<MavenPublication>(publicationName) {
                from(components["release"])
                artifact(tasks["androidSourcesJar"])
                artifact(tasks["androidKdocsJar"])
                groupId = "$group"
                artifactId = moduleArtifactId
                version = "$moduleVersion"

                pom {
                    description.set(project.description)
                    url.set(moduleWebsiteUrl)
                }
            }
        }
    }

    signing {
        sign(publishing.publications[publicationName])
    }

    bintray {
        setPublications(publicationName)
        user = bintrayUser
        key = bintrayKey
        publish = true

        pkg.apply {
            repo = "maven"
            name = "$group.${moduleArtifactId}"
            userOrg = "minotsu"
            setLicenses("MIT")
            vcsUrl = moduleVcsUrl
            websiteUrl = moduleWebsiteUrl
            desc = description

            version.apply {
                name = "$moduleVersion"
            }
        }
    }

    tasks {
        named("sign${publicationName.capitalize()}Publication") {
            group = "publishing"
        }
    }
}
