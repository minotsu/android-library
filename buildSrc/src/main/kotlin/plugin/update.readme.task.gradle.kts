/*******************************************************************************
 * © 2020 美濃通
 *
 * このソフトウェアはMITライセンスの下でリリースされています。
 * http://opensource.org/licenses/mit-license.php
 ******************************************************************************/

package plugin

import Version

internal val artifactId: String by project
internal val majorVersion = property("majorVersion").toString().toInt()
internal val minorVersion = property("minorVersion").toString().toInt()
internal val patchVersion = property("patchVersion").toString().toInt()
internal val moduleVersion = Version(majorVersion, minorVersion, patchVersion)

tasks {
    register("updateReadme") {
        group = "documentation"

        doLast {
            ant.withGroovyBuilder {
                "replaceregexp"(
                    "match" to "net\\.minotsu\\.android\\:${Regex.escape(artifactId)}\\:([0-9\\.]+)",
                    "replace" to "net.minotsu.android:$artifactId:$moduleVersion",
                    "flags" to "g",
                    "byline" to "true"
                ) {
                    "fileset"("dir" to projectDir, "includes" to "README.md")
                }
            }
        }
    }
}
