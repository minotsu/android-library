/*******************************************************************************
 * © 2020 美濃通
 *
 * このソフトウェアはMITライセンスの下でリリースされています。
 * http://opensource.org/licenses/mit-license.php
 ******************************************************************************/

package plugin

plugins {
    id("org.jetbrains.dokka")
}

tasks {
    dokka {
        outputFormat = "html"
    }
}
