/*******************************************************************************
 * © 2020 美濃通
 *
 * このソフトウェアはMITライセンスの下でリリースされています。
 * http://opensource.org/licenses/mit-license.php
 ******************************************************************************/

package plugin

import API_LEVEL
import BUILD_TOOLS_VERSION
import DEFAULT_CONSUMER_PROGUARD_FILES
import DEVELOPMENT_STORE_FILE
import KOTLIN_JVM_TARGET
import MIN_SDK_VERSION
import SOURCE_COMPATIBILITY
import TARGET_COMPATIBILITY
import TARGET_SDK_VERSION
import Version
import function.signingConfigs
import function.test

internal val majorVersion = property("majorVersion").toString().toInt()
internal val minorVersion = property("minorVersion").toString().toInt()
internal val patchVersion = property("patchVersion").toString().toInt()
internal val moduleVersion = Version(majorVersion, minorVersion, patchVersion)

plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("org.jetbrains.kotlin.android.extensions")
    id("plugin.update.readme.task")
    id("plugin.ktlint")
    id("plugin.bintray")
}

android {
    compileSdkVersion(API_LEVEL)
    buildToolsVersion = BUILD_TOOLS_VERSION

    defaultConfig {
        versionCode = moduleVersion.code
        versionName = "$moduleVersion"
        setMinSdkVersion(MIN_SDK_VERSION)
        setTargetSdkVersion(TARGET_SDK_VERSION)
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        testInstrumentationRunnerArgument("clearPackageData", "true")
        consumerProguardFiles(*DEFAULT_CONSUMER_PROGUARD_FILES)
    }

    signingConfigs(file(DEVELOPMENT_STORE_FILE))

    compileOptions {
        sourceCompatibility = SOURCE_COMPATIBILITY
        targetCompatibility = TARGET_COMPATIBILITY
    }

    kotlinOptions {
        jvmTarget = KOTLIN_JVM_TARGET
    }

    testOptions {
        execution = "ANDROIDX_TEST_ORCHESTRATOR"
        animationsDisabled = true

        unitTests.apply {
            isIncludeAndroidResources = true
        }
    }

    buildTypes {
        register("espresso") {
            initWith(named("release").get())
        }

        all {
            signingConfig = signingConfigs.single()
        }
    }

    testBuildType = "espresso"

    sourceSets.all {
        java.srcDirs(file("src/${name}/kotlin"))
    }

    libraryVariants.all {
        generateBuildConfigProvider.get().enabled = false
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(kotlin("stdlib"))
    test()
}
