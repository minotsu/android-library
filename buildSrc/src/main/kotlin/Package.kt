/*******************************************************************************
 * © 2020 美濃通
 *
 * このソフトウェアはMITライセンスの下でリリースされています。
 * http://opensource.org/licenses/mit-license.php
 ******************************************************************************/

object Pkg {

    private interface Group {
        val groupId: String
        val version: String
    }

    object Androidx {
        object Fragment : Group {
            override val groupId = "androidx.fragment"
            override val version = "1.2.4"
            val fragmentKtx = pkg("fragment-ktx")
            val fragmentTesting = pkg("fragment-testing")
        }

        object Test : Group {
            override val groupId = "androidx.test"
            override val version = "1.2.0"
            val coreKtx = pkg("core-ktx")
            val orchestrator = pkg("orchestrator")
            val rules = pkg("rules")

            object Espresso : Group {
                override val groupId = "androidx.test.espresso"
                override val version = "3.2.0"
                val espressoCore = pkg("espresso-core")
            }

            object Ext : Group {
                override val groupId = "androidx.test.ext"
                override val version = "1.1.1"
                val junitKtx = pkg("junit-ktx")
            }

            object Uiautomator : Group {
                override val groupId = "androidx.test.uiautomator"
                override val version = "2.2.0"
                val uiautomator = pkg("uiautomator")
            }
        }
    }

    object Org {
        object Assertj : Group {
            override val groupId = "org.assertj"
            override val version = "3.16.1"
            val assertjCore = pkg("assertj-core")
        }

        object Robolectric : Group {
            override val groupId = "org.robolectric"
            override val version = "4.3.1"
            val robolectric = pkg("robolectric")
        }
    }

    private fun <T : Group> T.pkg(artifactId: String): String = "$groupId:$artifactId:$version"
}
