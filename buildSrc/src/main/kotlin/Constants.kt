/*******************************************************************************
 * © 2020 美濃通
 *
 * このソフトウェアはMITライセンスの下でリリースされています。
 * http://opensource.org/licenses/mit-license.php
 ******************************************************************************/

import org.gradle.api.JavaVersion

internal const val API_LEVEL = 29
internal const val BUILD_TOOLS_VERSION = "29.0.3"
internal const val MIN_SDK_VERSION = 23
internal const val TARGET_SDK_VERSION = 29
internal val DEFAULT_CONSUMER_PROGUARD_FILES = arrayOf("consumer-rules.pro")
internal const val DEVELOPMENT_STORE_FILE = "keystore.jks"
internal val SOURCE_COMPATIBILITY = JavaVersion.VERSION_1_8
internal val TARGET_COMPATIBILITY = JavaVersion.VERSION_1_8
internal val KOTLIN_JVM_TARGET = JavaVersion.VERSION_1_8.toString()
