/*******************************************************************************
 * © 2020 美濃通
 *
 * このソフトウェアはMITライセンスの下でリリースされています。
 * http://opensource.org/licenses/mit-license.php
 ******************************************************************************/

plugins {
    `kotlin-dsl`
}

repositories {
    google()
    jcenter()
    gradlePluginPortal()
}

dependencies {
    implementation("com.android.tools.build:gradle:4.0.0")
    implementation(kotlin("gradle-plugin", "1.3.72"))
    implementation("com.github.ben-manes:gradle-versions-plugin:0.28.0")
    implementation("org.jlleitschuh.gradle:ktlint-gradle:9.2.1")
    implementation("org.jetbrains.dokka:dokka-android-gradle-plugin:0.9.18")
    implementation("com.jfrog.bintray.gradle:gradle-bintray-plugin:1.8.5")
}
