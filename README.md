[![official](https://img.shields.io/badge/official-URL-orange.svg)](https://bitbucket.org/minotsu/android-library/src/master/)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://spdx.org/licenses/MIT.html)
# 美濃通Androidライブラリ

## ユニットテスト
```shell script
./gradlew connectedAndroidTest
```
