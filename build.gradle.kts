/*******************************************************************************
 * © 2020 美濃通
 *
 * このソフトウェアはMITライセンスの下でリリースされています。
 * http://opensource.org/licenses/mit-license.php
 ******************************************************************************/

allprojects {
    repositories {
        google()
        jcenter()
    }
}

plugins {
    id("com.github.ben-manes.versions")
    id("plugin.ktlint")
}

tasks {
    register<Delete>("clean") {
        group = "build"
        delete(rootProject.buildDir)
    }
}
