[![official](https://img.shields.io/badge/official-URL-orange.svg)](https://bitbucket.org/minotsu/android-library/src/master/viewbinding/)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://spdx.org/licenses/MIT.html)
![Bintray](https://img.shields.io/bintray/v/minotsu/maven/net.minotsu.android.viewbinding)
# ViewBindingライブラリ

## 利用方法
implementation("net.minotsu.android:viewbinding:0.1.0")
