/*******************************************************************************
 * © 2020 美濃通
 *
 * このソフトウェアはMITライセンスの下でリリースされています。
 * http://opensource.org/licenses/mit-license.php
 ******************************************************************************/

plugins {
    id("plugin.android.library")
}

android {
    buildFeatures {
        viewBinding = true
    }
}

dependencies {
    implementation(Pkg.Androidx.Fragment.fragmentKtx)
    espressoImplementation(Pkg.Androidx.Fragment.fragmentTesting)
}
