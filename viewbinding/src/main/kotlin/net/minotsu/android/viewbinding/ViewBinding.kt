/*******************************************************************************
 * © 2020 美濃通
 *
 * このソフトウェアはMITライセンスの下でリリースされています。
 * http://opensource.org/licenses/mit-license.php
 ******************************************************************************/

@file:Suppress("RedundantVisibilityModifier")

package net.minotsu.android.viewbinding

import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

/**
 * ViewBindingの委譲プロパティを返す.
 *
 * FragmentでViewBindingの参照をプロパティとして持つとき、onDestroyViewでその参照を外さないとリークする。
 * それを意識する必要がないViewBindingの委譲プロパティを返す。
 *
 * @sample net.minotsu.android.viewbinding.ViewBindingTestFragment
 */
public inline fun <reified T : ViewBinding> viewBinding(): ReadOnlyProperty<Fragment, T> {
    return object : ReadOnlyProperty<Fragment, T> {
        override fun getValue(thisRef: Fragment, property: KProperty<*>): T {
            val key = property.name.hashCode()

            @Suppress("UNCHECKED_CAST")
            return thisRef.requireView().getTag(key) as? T
                ?: bind(thisRef.requireView()).also { thisRef.requireView().setTag(key, it) }
        }

        private fun bind(view: View): T {
            return T::class.java.getMethod("bind", View::class.java).invoke(null, view) as T
        }
    }
}
