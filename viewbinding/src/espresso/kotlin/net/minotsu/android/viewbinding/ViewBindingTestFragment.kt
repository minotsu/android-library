/*******************************************************************************
 * © 2020 美濃通
 *
 * このソフトウェアはMITライセンスの下でリリースされています。
 * http://opensource.org/licenses/mit-license.php
 ******************************************************************************/

package net.minotsu.android.viewbinding

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import net.minotsu.android.viewbinding.databinding.ViewBindingTestFragmentBinding

internal class ViewBindingTestFragment : Fragment(R.layout.view_binding_test_fragment) {

    private val binding: ViewBindingTestFragmentBinding by viewBinding()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.textView.isVisible = true
    }
}
