/*******************************************************************************
 * © 2020 美濃通
 *
 * このソフトウェアはMITライセンスの下でリリースされています。
 * http://opensource.org/licenses/mit-license.php
 ******************************************************************************/

package net.minotsu.android.viewbinding

import androidx.fragment.app.FragmentFactory
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import org.assertj.core.api.Assertions
import org.junit.Test
import org.junit.runner.RunWith

@SmallTest
@RunWith(AndroidJUnit4::class)
class ViewBindingTest {

    @Test
    fun viewBindingIsNullAfterDetachFragment() {
        val scenario =
            launchFragmentInContainer<ViewBindingTestFragment>(factory = FragmentFactory())

        scenario.onFragment {
            it.parentFragmentManager.beginTransaction().detach(it).commitNow()
            Assertions.assertThat(it.view?.getTag("binding".hashCode())).isNull()
        }
    }
}
